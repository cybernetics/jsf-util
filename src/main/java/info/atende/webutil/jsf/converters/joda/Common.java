package info.atende.webutil.jsf.converters.joda;

import javax.faces.component.UIComponent;
import java.util.Map;

/**
 * Acoes Comuns aos conversores
 * Criado por giovanni.
 * Data: 28/04/12
 * Hora: 18:23
 */
public class Common {
    public static String getPattern(UIComponent component){
        Map<String, Object> properties = component.getAttributes();
        String pattern = (String) properties.get("pattern");
        return  pattern;
    }
}
